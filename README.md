# Broot Nord Theme

Broot: https://dystroy.org/broot/
Nord Theme: https://www.nordtheme.com/

### This is my attempt at making a Nord Theme for Broot.
### You will need a terminal capable of 24 bit True Color, I use Alacritty and Kitty.

### To use just copy the contents of the broot.skin file the end of your conf.toml

Alacritty: https://github.com/alacritty/alacritty
Kitty: https://sw.kovidgoyal.net/kitty/
